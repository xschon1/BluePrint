**Author**: Bc. Martin Schön
**Mail**: schon.xmartin@gmail.com
**Dataset**: Data provided in correlation with an assignment from Blueprint Power.

# CBL Verification
## Code
This work contains Flask Python 3.11 application with RestAPI access. It creates a localhost server runnable via Docker container on a localhost machine. There is a necessity to use the PostgreSQL database for correct functionalities and usage of config files with credentials to connect to the application.
### Database
This solution uses PostgreSQL tables. Those are automatically created and filled in with the data stored in the attached .csv files. I have also reworked the holidays pdf to a .csv file for easier manipulation. The full process of creating and filling the tables, as well as the code necessary for this functionality, is stored in the database_handling directory. 

After starting the application, it runs a check for data connection and existing data-tables necessary. If they do not exist, they are created via DDL scripts (.sql files) and filled in with corresponding data. The database connection is then further used during the whole runtime of an application, as this offers us better opportunities to work with data.

### Code parts
The main function of the application is app.py, which represents the body of a server. It also contains code for input validations and request handling. 

Apart from that, the database_handling part was mentioned and the last one is cbl_calculations, which contains scripts for processing energy and events data. events.py also calculates the main part of this assignment in the form of CBL calculations.

#### CBL calculations
CBL calculations are split into two similar functions - `weekday_classification` and `weekend_classification`. Both of those are following instructions mentioned in the customer-baseline-load-procedure.pdf. They select fitting windows depending on the definition and then calculate the CBL basis based on the instructions about historical usage. For closer information, read the .pdf information.

### Functionality
This application is shipped with a docker image. To run the application we can use a command 

    docker-compose up --build -d
which will start the application in accordance with the docker-compose.

After starting the application, we can send GET requests to its API endpoint (on localhost):
http://127.0.0.1:5000/api/baseline
The application takes 5 input parameters

 - a.building_id: ID of the building for baseline calculation, 
 - b.timezone: Building timezone in a pytz format, 
 - c. start: Baseline
   event window start in UTC, 
 - d. end: Baseline event window ends in UTC,
  - e. granularity: Output time series granularity. Supported are 5min,
   15min, 1hour.

Those parameters can be included in the link **or** in the JSON body of a request.

### Outputs
The output of a request is a list of JSONs containing information about calculated CBLs based on input parameters. 

    {
		"building_id": "Building E",
		"timestamp": "2023-02-28T09:00:00",
		"value": 5205044317.552867
	},
Here is an example of output JSON. The application returns data in a right-labelled format. 

### Possible improvements
These are possible improvements and my rough ideas for implementing them. Unfortunately, due to a lack of time in the past week, I have not been able to implement all of them.
**Database QoL changes** - splitting the energy data table into two tables (building and energy) and creating Foreign Key one-to-many relationship between them
**Storing final data into the database** - after calculating the output, we can store data in the database and maybe even implement checks for future re-calculations of the same event windows. 
**Tests and checks of outputs** - I didn't have time for proper testing of calculations. I checked some and calculated a few values using another instance of Python and data, but more proper testing couldn't hurt.
**Django implementation** - At first, I was considering using Django for this project, to improve my skills in it, but in the end, I didn't have much time, so I tested Flask instead. Django could be fun and good for the task as well!

## Further questions
If you have any further questions about my code or thought process, don't hesitate to contact me via mail mentioned in the header of this documentation. It was a pleasure to work on this assignment and I hope you enjoyed my work.