import os
import logging

class Config:
    DEBUG = False
    TESTING = False

    logger = logging.getLogger('urbanGUI')
    logging.basicConfig(filename="logging.txt",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

class DevelopmentConfig(Config):
    DEBUG = True

    DB_HOST=''
    DB_NAME=''
    DB_USER=''
    DB_PASSWORD=''
    DB_PORT=''
    CONNECTION_STRING = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

class ProductionConfig(Config):
    DB_HOST=''
    DB_NAME=''
    DB_USER=''
    DB_PASSWORD=''
    DB_PORT=''
    CONNECTION_STRING = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

config_env = os.environ.get('FLASK_ENV', 'development')

if config_env == 'production':
    app_config = ProductionConfig()
else:
    app_config = DevelopmentConfig()