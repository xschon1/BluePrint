from config import app_config
from flask import Flask, jsonify, request
from database_handling import database_handler
from datetime import datetime
import pytz
from dateutil import parser
from cbl_calculations.events import Events
from cbl_calculations.energy import Energy

app = Flask(__name__)
database_handler.check_database()

@app.route('/')
def hello():
    return 'Hello, World!'

# Route to get baseline data
@app.route('/api/baseline', methods=['GET'])
def get_baseline_data():
    # Allow input to be send via JSON or via request ARGS
    request_data = request.json
    building_id = request_data.get('building_id') if request_data else None
    timezone = request_data.get('timezone') if request_data else None
    start = request_data.get('start') if request_data else None
    end = request_data.get('end') if request_data else None
    granularity = request_data.get('granularity') if request_data else None

    required_arguments = [building_id, timezone, start, end, granularity]
    if not all(required_arguments):
        building_id = request.args.get('building_id')
        timezone = request.args.get('timezone')
        start = request.args.get('start')
        end = request.args.get('end')
        granularity = request.args.get('granularity')
        required_arguments = [building_id, timezone, start, end, granularity]

    # Check if all required args are sent
    if not all(required_arguments):
        args_strings = ["building_id", "timezone", "start", "end", "granularity"]
        return jsonify({"error": f"Missing some arguments.\nYou need to send: {args_strings}"}), 400
        

    energy = Energy()
    # DATA VALIDATION
    try:
        tmz = pytz.timezone(timezone)
        if not (energy.check_buildings_timestamps(building_id, tmz)):
            return jsonify({'error': 'This combination of building and timezone is not valid'}), 400
    except pytz.UnknownTimeZoneError:
        return jsonify({'error': 'Invalid timezone'}), 400

    try:
        start_datetime = parser.isoparse(start)
        end_datetime = parser.isoparse(end)
        if start_datetime > end_datetime:
            return (jsonify({"error": "End time has to be after start time"}))
    except ValueError:
        return jsonify({"error": "Invalid start or end timestamp format"}), 400

    supported_granularities = ['5min', '15min', '1hour']
    if granularity not in supported_granularities:
        return jsonify({'error': 'Unsupported granularity'}), 400

    app_config.logger.info(f"Got HTTP request: Building: {building_id}, Timezone: {timezone}, Start: {start}, End: {end}, Granularity {granularity}")

    events = Events()
    is_weekday = True if (0 <= start_datetime.weekday() <= 4) else False

    if is_weekday:
        cbl_values = events.weekday_classification(start, end, building_id, granularity, energy)
    else:
        cbl_values = events.weekend_classification(start, end, building_id, granularity, energy)

    energy.evaluate_historical_data(building_id, cbl_values)

    return jsonify(cbl_values)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')