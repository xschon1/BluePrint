import pandas as pd
import pytz
from database_handling import database_handler


class Energy:
    def __init__(self) -> None:
        self.energy = database_handler.retrieve_data("SELECT * FROM energy;")
        self.energy["timestamp"] = pd.to_datetime(self.energy["timestamp"])

    def check_buildings_timestamps(self, building_name: str, building_timezone) -> bool:
        """
        Check if timestamp is assigned to building in the database
        Returns True (if it is) or False (otherwise)
        """
        my_building = self.energy[self.energy["building_id"] == building_name]
        request_timezone = pytz.timezone(my_building["timezone"].iloc[0])
        return request_timezone == building_timezone

    def get_building_data(self, building_id: str) -> pd.DataFrame:
        return self.energy[self.energy.building_id == building_id]

    def evaluate_historical_data(self, building_id: str, cbl_values):
        building_energy = self.energy[self.energy["building_id"] == building_id]
        abs_e = 0
        for i in cbl_values:
            true_value = building_energy[
                building_energy["timestamp"] == pd.to_datetime(i["timestamp"], utc=True)
            ].cumulative_energy_kwh.iloc[0]
            print(f"True {true_value} and Predicted {i['value']}")
            print(f"Diff {true_value - i['value']}")
            abs_e += true_value - i['value']
        print(f"ABSE {abs_e}")
        pass
