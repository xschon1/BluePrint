import pandas as pd
from database_handling import database_handler
from datetime import datetime, timedelta
from cbl_calculations.energy import Energy
from pytz import utc


class Events:
    def __init__(self) -> None:
        self.events = database_handler.retrieve_data("SELECT * FROM events;")
        self.holidays = database_handler.retrieve_data("SELECT * FROM holidays;")
        self.holidays["day"] = pd.to_datetime(self.holidays["day"], format="%B %d %Y")

        # Parse datetime to UTC for easier comparisson
        self.events["event_start"] = pd.to_datetime(
            self.events["event_start"], utc=True
        )
        self.events["event_end"] = pd.to_datetime(self.events["event_end"], utc=True)
        self.events = self.events.drop({"timezone"}, axis=1)

    def find_events_window(self, start: str, end: str) -> pd.DataFrame:
        """
        Find all events that happened in a given window
        :start and :end - two str ISO8601 datetimes for events window
        """
        # Select only events active in the window
        start_date = pd.to_datetime(start)
        end_date = pd.to_datetime(end)
        active_events = self.events[
            (self.events["event_start"].__ge__(start_date))
            & (self.events["event_end"].__le__(end_date))
        ]
        return active_events

    def get_kw_values(
        self, use_date, hours_to_check, building_energy, granularity="1hour"
    ):
        kw_values = []
        current_date = use_date
        while current_date <= (use_date + timedelta(hours=hours_to_check)):
            check_date = pd.Timestamp(current_date, tz=utc)
            kw_values.append(
                building_energy[
                    building_energy.timestamp == check_date
                ].cumulative_energy_kwh.iloc[0]
            )
            if granularity == "1hour":
                current_date += timedelta(hours=1)
            if granularity == "15min":
                current_date += timedelta(minutes=15)
            if granularity == "5min":
                current_date += timedelta(minutes=5)

        return kw_values

    def weekday_classification(
        self,
        start: str,
        end: str,
        building: str,
        granularity: str,
        energy: Energy,
        past_days: int = 30,
        eliminate_low: bool = True,
        days_return: bool = False,
    ) -> list:
        """
        Calculate which days are suitable for further calculations of CBL for classic week day
        Either return list of dates containing data about suitable days or full calculation right-labeled data (choose by days_return)
        """
        start_date = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
        end_date = datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
        building_energy = energy.get_building_data(building)

        to_evaluate_days = [
            (start_date - timedelta(days=i)) for i in range(1, past_days + 1)
        ]

        hours_to_check = int((end_date - start_date).total_seconds()) // 3600
        current_date = start_date

        # Determine participants peak hourly load and create average
        max_values = [-1 for _ in range(hours_to_check + 1)]
        i = 0
        while current_date <= end_date:
            check_date = pd.Timestamp(current_date, tz=utc)
            for history_day in range(1, 31):
                current_value = building_energy[
                    building_energy.timestamp
                    == check_date - timedelta(days=history_day)
                ].cumulative_energy_kwh.iloc[0]
                if current_value > max_values[i]:
                    max_values[i] = current_value

            current_date += timedelta(hours=1)
            i += 1

        seed_average = sum(max_values) / len(max_values)
        seed_strength = 0
        eligible_days = []

        # Select ALL events mentioned in points 2 through 5
        ignored_events = self.events[
            (
                self.events.program_name.isin(["DLRP", "Term-DLM", "Auto-DLM", "CSRP"])
                & (self.events.utility_id.__eq__("ConEd"))
            )
            | (
                (self.events.program_name.isin(["SCR", "EDRP", "DDRP"]))
                & (self.events.ISO == "NYISO")
            )
        ]["event_start"]
        for evaluate_day in to_evaluate_days[1:]:
            # 1. eliminate holidays and weekends
            if evaluate_day in self.holidays["day"].dt.date.values:
                continue  # holidays
            if evaluate_day.weekday() >= 5:
                continue  # weekend

            # 2. 3. 4. 5. Eliminate DLPR, CSRP, Term-DLM or Auto-DLM events by ConEd and the day before
            # Eliminate SCR, EDRP, TDRP events from NYISO and the day before
            if (
                evaluate_day.date() in ignored_events.dt.date.values
                or (evaluate_day.date() - timedelta(days=1))
                in ignored_events.dt.date.values
            ):
                continue

            # 6. Create daily average and 7. eliminate if it is less than 25% of seed
            if eliminate_low:
                kw_values = self.get_kw_values(
                    evaluate_day, hours_to_check, building_energy
                )
                daily_average = sum(kw_values) / len(kw_values)

                if daily_average < (seed_average * 0.25):
                    continue

                # Add daily average to the total average of seed
                seed_average = (daily_average + seed_average * seed_strength) / (
                    seed_strength + 1
                )
                seed_strength += 1  # add new element, used average becomes stronger

            eligible_days.append(evaluate_day)

            if len(eligible_days) == 10:
                break

        if days_return:
            return eligible_days

        if len(eligible_days) < 10:
            # In case we didnt get 10 days remove reqs and get new values
            eligible_days = self.weekday_classification(
                start,
                end,
                building,
                energy,
                past_days=99999,
                eliminate_low=False,
                days_return=True,
            )

        # STEP 2. Eliminate 5 smallest averages
        day_kw_value = {}
        for day in eligible_days:
            kw_values = self.get_kw_values(day, hours_to_check, building_energy)
            daily_average = sum(kw_values) / len(kw_values)
            day_kw_value[day] = daily_average

        sorted_dict = sorted(day_kw_value.items(), key=lambda x: x[1], reverse=True)
        top_5 = dict(sorted_dict[:5])

        # Calculate the final values - for top days, based on granularity and return
        top_relevant_days = list(top_5.keys())
        final_values = []
        for day in top_relevant_days:
            kw_values = self.get_kw_values(
                day, hours_to_check, building_energy, granularity
            )
            final_values.append(kw_values)

        expected_averages = [sum(items) / len(items) for items in zip(*final_values)]

        current_date = start_date
        time_series = []
        i = 0
        while current_date <= end_date:
            time_series.append(
                {"building_id": building, "timestamp": current_date.isoformat(), "value": expected_averages[i]}
            )
            i += 1
            if granularity == "1hour":
                current_date += timedelta(hours=1)
            if granularity == "15min":
                current_date += timedelta(minutes=15)
            if granularity == "5min":
                current_date += timedelta(minutes=5)
        return time_series

    def weekend_classification(
        self,
        start: str,
        end: str,
        building: str,
        granularity: str,
        energy: Energy,
):
        start_date = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
        end_date = datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
        building_energy = energy.get_building_data(building)
        hours_to_check = int((end_date - start_date).total_seconds()) // 3600

        # last 3 same weekdays
        to_evaluate_days = [
            (start_date - timedelta(days=-i)) for i in range(7,22,7)  
        ]

        # STEP 2. Eliminate 5 smallest averages
        day_kw_value = {}
        for day in to_evaluate_days:
            kw_values = self.get_kw_values(day, hours_to_check, building_energy)
            daily_average = sum(kw_values) / len(kw_values)
            day_kw_value[day] = daily_average

        sorted_dict = sorted(day_kw_value.items(), key=lambda x: x[1], reverse=True)
        top_2 = dict(sorted_dict[:2])

        top_relevant_days = list(top_2.keys())
        final_values = []
        for day in top_relevant_days:
            kw_values = self.get_kw_values(
                day, hours_to_check, building_energy, granularity
            )
            final_values.append(kw_values)

        expected_averages = [sum(items) / len(items) for items in zip(*final_values)]

        current_date = start_date
        time_series = []
        i = 0
        while current_date <= end_date:
            time_series.append(
                {"building_id": building, "timestamp": current_date.isoformat(), "value": expected_averages[i]}
            )
            i += 1
            if granularity == "1hour":
                current_date += timedelta(hours=1)
            if granularity == "15min":
                current_date += timedelta(minutes=15)
            if granularity == "5min":
                current_date += timedelta(minutes=5)
        return time_series

