CREATE TABLE events (
    id SERIAL PRIMARY KEY,
    iso VARCHAR(50),
    utility_id VARCHAR(50),
    program_name VARCHAR(50),
    event_start TIMESTAMP WITH TIME ZONE,
    event_end TIMESTAMP WITH TIME ZONE,
    timezone VARCHAR(50)
);