CREATE TABLE energy (
    id SERIAL PRIMARY KEY,
    building_id VARCHAR(50),
    timezone VARCHAR(50),
    timestamp TIMESTAMP WITH TIME ZONE,
    cumulative_energy_kwh NUMERIC
);