import os

import pandas as pd
from sqlalchemy import create_engine, MetaData
from sqlalchemy.exc import OperationalError

from config import app_config


# create engine for the entire database handling
try:
    engine = create_engine(app_config.CONNECTION_STRING)
    with engine.connect() as connection:
        result = connection.execute("SELECT 1")
        app_config.logger.info("Connection to the database is successful.")
except OperationalError as e:
    app_config.logger.info("Error connecting to the database:", e)


def prepare_tables(to_create):
    """Create SQL table from their corresponding .csv files."""
    dir_path = os.path.dirname(os.path.abspath(__file__))

    for table_name in to_create:
        # Create table via DDL SQL queries
        with open(os.path.join(dir_path, f"queries/DDL/{table_name}.sql"), "r") as ddl:
            ddl_query = ddl.read()
            engine.execute(ddl_query)

        # Fill data to the tables based on csv files
        table_data = pd.read_csv(os.path.join(dir_path, f"data/{table_name}.csv"))
        table_data.to_sql(table_name, engine, if_exists="replace", index=False)


def check_database():
    """
    Check if database has all tables necessary for this work.
    If they do not exist, create them based on predefined DDL and fill data in.
    """
    # Check for all possible tables and if they DO exist
    possible_tables = ["energy", "events", "holidays"]

    metadata = MetaData()
    metadata.reflect(bind=engine)
    table_names = engine.table_names()

    add_tables = [pt for pt in possible_tables if pt not in table_names]
    prepare_tables(add_tables)


def retrieve_data(query: str) -> pd.DataFrame:
    """
    Function loading data from database into a pandas dataframe.
    :query - SQL query string syntax
    :returns - pd.DataFrame containing data from table
    """
    return pd.read_sql(query, engine)
    

